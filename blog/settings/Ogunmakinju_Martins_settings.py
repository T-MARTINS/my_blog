import sys

from .settings import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'NAME': 'hipsterblog',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'root',
        'PASSWORD': '00067764',
        'HOST': 'localhost',  # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    },
}
