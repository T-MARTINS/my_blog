from django.contrib import admin

# Register your models here.
from .models import Comment


class CommentAdmin(admin.ModelAdmin):
    list_display = ('email', 'timestamp', 'comments')

    def comments(self, obj):
        return obj.content


admin.site.register(Comment, CommentAdmin)
