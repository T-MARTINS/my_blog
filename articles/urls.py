from django.urls import path, re_path
from articles import views
from .feeds import LatestEntriesFeed

app_name = 'articles'

urlpatterns = [
    path('', views.index, name='home'),
    path('contact_us/', views.contact_us, name='contact_us'),
    path('newsletter/', views.control_newsletter, name='control_newsletter'),
    path('about_us/', views.about, name='about'),
    path('register/', views.register, name='register'),
    path('profile/', views.profile_view, name='profile'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('terms/', views.terms, name='terms'),
    path('advert/', views.advert, name='advert'),
    path('feed/', LatestEntriesFeed()),
    path('ads.txt/', views.authorized_digital_sellers, name='Authorized_Digital_Sellers'),
    path('<slug:slug>/', views.article_detail, name='detail'),
    path('video/<slug:slug>/', views.video_detail, name='video_detail'),
    re_path('category/(?P<cat_name>[^/]+)/$', views.article_list, name='list'),
    re_path('option/(?P<status>[^/]+)/$', views.options, name='option')

]
