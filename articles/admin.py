from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Article, Category, LikeViews, User, Video
from .models import NewsletterUser, Newsletter


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'category_ref', 'status', 'created_on')
    list_filter = ("status",)
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}


class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at')


class hipsterblogUserAdmin(UserAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email')


class NewsletterAdmin(admin.ModelAdmin):
    list_display = ("email", "date_added",)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Category)
admin.site.register(LikeViews)
admin.site.register(User, hipsterblogUserAdmin)
admin.site.register(NewsletterUser, NewsletterAdmin)
admin.site.register(Newsletter)

admin.site.site_header = "Hipsterblog Administration Panel"