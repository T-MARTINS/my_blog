import requests
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import get_template
from blog.settings import production_settings
from .models import Article, LikeViews, User, Category, NewsletterUser, Newsletter, Video
from django.contrib.contenttypes.models import ContentType
from comments.forms import CommentForm
from comments.models import Comment
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, Http404, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from articles.utils import get_client_ip
from taggit.models import Tag
from articles.forms import (
    NewsletterUserSignUpForm,
    NewsletterCreationForm,
    UserRegisterForm,
    UserLoginForm
)
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)


def index(request):
    articles = Article.objects.all().order_by('-created_on')[:10]
    category = Article.objects.filter(category_ref__name='category').order_by('-created_on')
    news = Article.objects.filter(category_ref__name='politics').order_by('-created_on')[:5]
    life = Article.objects.filter(category_ref__name='business').order_by('-created_on')[:2]
    sport1 = Article.objects.filter(category_ref__name='sports').order_by('-created_on')[:1]
    sport2 = Article.objects.filter(category_ref__name='sports').order_by('-created_on')[1:3]
    sport3 = Article.objects.filter(category_ref__name='sports').order_by('-created_on')[3:5]
    ent1 = Article.objects.filter(category_ref__name='entertainment').order_by('-created_on')[:1]
    ent2 = Article.objects.filter(category_ref__name='entertainment').order_by('-created_on')[1:2]
    ent3 = Article.objects.filter(category_ref__name='entertainment').order_by('-created_on')[2:3]
    ent4 = Article.objects.filter(category_ref__name='entertainment').order_by('-created_on')[3:4]
    pic1 = Article.objects.filter(category_ref__name='entertainment').order_by('-created_on')[:4]
    pic2 = Article.objects.filter(category_ref__name='entertainment').order_by('-created_on')[4:7]
    tech = Article.objects.filter(category_ref__name='world').order_by('-created_on')[:1]
    tech2 = Article.objects.filter(category_ref__name='world').order_by('-created_on')[1:3]
    politics = Article.objects.filter(category_ref__name='news').order_by('-created_on')[:1]
    instance = Article.objects.filter(category_ref__name='news').order_by('-created_on')[1:4]
    obj = Article.objects.all().order_by('-created_on')[1:8]
    post = Article.objects.all().order_by('-created_on')[:1]
    post1 = Article.objects.all().order_by('-created_on')[1:2]
    post2 = Article.objects.all().order_by('-created_on')[2:3]
    post3 = Article.objects.all().order_by('-created_on')[3:4]
    widget = Article.objects.all().order_by('-created_on')[:5]
    video = Video.objects.all()[:3]
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    forms = NewsletterUserSignUpForm(request.POST or None)
    if forms.is_valid():
        instance = forms.save(commit=False)
        if NewsletterUser.objects.filter(email=instance.email).exists():
            messages.warning(request, "Email Address Already Exists", "alert alert-warning alert-dismissible")
        else:
            instance.save()
            messages.success(request, "Congratulation!, Your Subscription is Successful.",
                             "alert alert-success alert-dismissible")
            subject = "Thank you for joining our Newsletter family"
            from_email = production_settings.EMAIL_HOST_USER
            to_email = [instance.email]
            message = EmailMultiAlternatives(subject=subject, from_email=from_email, to=to_email)
            html_template = get_template("signup_newsletter.html").render()
            message.attach_alternative(html_template, "text/html")
            message.send()
    context = {
        "post": post, "post1": post1, "post2": post2, "post3": post3, "news": news, "obj": obj, "tech": tech,
        "tech2": tech2, "articles": articles, "category": category, "politics": politics, "instance": instance,
        "ent1": ent1, "ent2": ent2, "ent3": ent3, "ent4": ent4, "sport1": sport1, "sport2": sport2, "video": video,
        "sport3": sport3, "life": life, "widget": widget, 'categories': categories, "cate_1": cate_1, "cate_2": cate_2,
        "city_weather": city_weather, "forms": forms, "pic1": pic1, "pic2": pic2, "cate_more": cate_more
    }
    return render(request, 'index.html', context)


def article_detail(request, slug):
    instance = Article.objects.get(slug=slug)
    cat_id = instance.category_ref.id
    related_post = Article.objects.filter(category_ref_id=cat_id)[1:7]
    obj = Article.objects.all().order_by('-created_on')[:10]
    category = Article.objects.filter(category_ref__name='category')
    common_tags = Article.tags.most_common()
    ip = get_client_ip(request)
    if not LikeViews.objects.filter(article_id=instance.id,
                                    action='view', ip=ip).exists():
        new_view = LikeViews()
        new_view.article = Article.objects.get(pk=instance.id)
        new_view.ip = ip
        new_view.action = 'view'
        new_view.save()
        instance.viewcount = instance.viewcount + 1
        instance.save()

    initial_data = {
        "content_type": instance.get_content_type,
        "object_id": instance.id
    }
    comment_form = CommentForm(request.POST or None, initial=initial_data)
    if comment_form.is_valid():
        c_type = comment_form.cleaned_data.get('content_type')
        content_type = ContentType.objects.get(model=c_type)
        obj_id = comment_form.cleaned_data.get('object_id')
        email = comment_form.cleaned_data.get('email')
        content_data = comment_form.cleaned_data.get("content")
        parent_obj = None
        try:
            parent_id = int(request.POST.get("parent_id"))
        except:
            parent_id = None
        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                parent_obj = parent_qs.first()
        new_comment, created = Comment.objects.get_or_create(
            email=email,
            content_type=content_type,
            object_id=obj_id,
            content=content_data,
            parent=parent_obj,
        )
        return HttpResponseRedirect(new_comment.content_object.get_absolute_url())
    comments = instance.comments[:4]
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    locurl = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(locurl.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "article": instance, "comments": comments, "comment_form": comment_form, "obj": obj, "category": category,
        "related_post": related_post, 'categories': categories, "common_tags": common_tags, "cate_1": cate_1,
        "cate_2": cate_2, "city_weather": city_weather, "cate_more": cate_more
    }
    return render(request, 'news-details.html', context)


def article_list(request, cat_name):
    categories = Category.objects.all()[:10]
    category = Article.objects.filter(category_ref__name=cat_name)
    cate = Article.objects.filter(category_ref__name=cat_name)[:1]
    cate_1 = Category.objects.all()[:5]
    cate_more = Category.objects.all()[11:]
    cate_2 = Category.objects.all()[6:]
    query = request.GET.get('q')
    if query:
        category = category.filter(
            Q(title__icontains=query) |
            Q(content__icontains=query) |
            Q(author__first_name__icontains=query) |
            Q(author__last_name__icontains=query)
        ).distinct()
    paginator = Paginator(category, 15) # Show 25 contacts per page.
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
         "categories": categories, "cate": cate, "cate_1": cate_1, "cate_2": cate_2,
         'page_obj': page_obj, "city_weather": city_weather, "cate_more": cate_more
    }
    return render(request, "listing.html", context)


def contact_us(request):
    articles = Article.objects.all().order_by('-created_on')[:5]
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "categories": categories, "city_weather": city_weather,
        "cate_1": cate_1, "cate_more": cate_more,
        "cate_2": cate_2,
        "articles": articles
    }
    return render(request, 'contact-us.html', context)


def about(request):
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "categories": categories, "city_weather": city_weather,
        "cate_1": cate_1, "cate_more": cate_more,
        "cate_2": cate_2
    }
    return render(request, 'about-us.html',  context)


def terms(request):
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "categories": categories, "city_weather": city_weather,
        "cate_1": cate_1, "cate_more": cate_more,
        "cate_2": cate_2
    }
    return render(request, 'terms.html',  context)


def advert(request):
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "categories": categories, "city_weather": city_weather,
        "cate_1": cate_1, "cate_more": cate_more,
        "cate_2": cate_2
    }
    return render(request, 'advert.html',  context)


def options(request, status):
    if request.method == 'GET':
        context = {}
        ip = get_client_ip(request)
        if status == 'like':
            if LikeViews.objects.filter(ip=ip, article_id=int(request.GET.get('article')),
                                        action='like').exists():
                context['status'] = 'like_exist'
                context['message'] = 'you have already like this article'
                return JsonResponse(context, status=200, safe=False)
            else:
                new_like = LikeViews()
                new_like.article = Article.objects.get(pk=int(request.GET.get('article')))
                new_like.ip = ip
                new_like.action = 'like'
                new_like.save()
                article = Article.objects.get(pk=int(request.GET.get('article')))
                article.likecount = article.likecount + 1
                article.save()
                context['like_count'] = article.likecount
                context['status'] = 'like'
                return JsonResponse(context, status=200, safe=False)
    else:
        pass


@login_required
def comment_delete(request, id):
    try:
        obj = Comment.objects.get(id=id)
    except:
        raise Http404
    if obj.user != request.user:
        response = HttpResponse("You do not have permission to do this.")
        response.status_code = 403
        return response
    if request.method == " POST":
        parent_obj_url = obj.content_object.get_absolute_url()
        obj.delete()
        messages.success(request, "This has been deleted.")
        return HttpResponseRedirect(parent_obj_url)
    context = {
        "object": obj,
    }
    return render(request, "confirm_delete.html", context)


def tagged(request, slug):
    tag = get_object_or_404(Tag, slug=slug)
    article = Article.objects.filter(tags=tag)
    context = {
        "tag": tag,
        "article": article,
    }
    return render(request, 'news-details.html', context)


def control_newsletter(request):
    form = NewsletterCreationForm(request.POST or None)

    if form.is_valid():
        instance = form.save()
        newsletter = Newsletter.objects.get(id=instance.id)
        if newsletter.status == "Published":
            subject = newsletter.subject
            body = newsletter.body
            from_email = production_settings.EMAIL_HOST_USER
            for email in Newsletter.email.all():
                send_mail(subject=subject, message=body, from_email=from_email, recipient_list=email,
                          fail_silently=False)
    context = {
            "form": form
    }
    return render(request, 'control_newsletter.html', context)


def register(request):
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get("password")
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect("/")
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "form": form, "city_weather": city_weather, "categories": categories,
        "cate_1": cate_1, "cate_2": cate_2, "cate_more": cate_more
    }
    return render(request, 'reg_form.html', context)


def login_view(request):
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect("/")
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(url.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "form": form, "city_weather": city_weather, "categories": categories,
        "cate_1": cate_1, "cate_2": cate_2, "cate_more": cate_more
    }
    return render(request, 'login.html', context)


def logout_view(request):
    logout(request)
    return redirect("/")


def profile_view(request):
    user = User.objects.filte.all
    context = {
        "user": user
    }
    return render(request, 'profile.html', context)


def video_detail(request, slug):
    instance = Video.objects.get(slug=slug)
    cat_id = instance.category_ref.id
    related_post = Video.objects.filter(category_ref_id=cat_id)[1:7]
    obj = Article.objects.all().order_by('-created_on')[:10]
    category = Video.objects.filter(category_ref__name='category')
    initial_data = {
        "content_type": instance.get_content_type,
        "object_id": instance.id
    }
    comment_form = CommentForm(request.POST or None, initial=initial_data)
    if comment_form.is_valid():
        c_type = comment_form.cleaned_data.get('content_type')
        content_type = ContentType.objects.get(model=c_type)
        obj_id = comment_form.cleaned_data.get('object_id')
        email = comment_form.cleaned_data.get('email')
        content_data = comment_form.cleaned_data.get("content")
        parent_obj = None
        try:
            parent_id = int(request.POST.get("parent_id"))
        except:
            parent_id = None
        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                parent_obj = parent_qs.first()
        new_comment, created = Comment.objects.get_or_create(
            email=email,
            content_type=content_type,
            object_id=obj_id,
            content=content_data,
            parent=parent_obj,
        )
        return HttpResponseRedirect(new_comment.content_object.get_absolute_url())
    comments = instance.comments[:4]
    categories = Category.objects.all()[:10]
    cate_more = Category.objects.all()[11:]
    cate_1 = Category.objects.all()[:5]
    cate_2 = Category.objects.all()[6:]
    locurl = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=43e8c934e7122d5b40a25fe6b66c7390'
    city = "lagos"
    r = requests.get(locurl.format(city)).json()
    city_weather = {
        'city': city,
        'temperature': r['main']['temp'],
        'description': r['weather'][0]['description'],
        'icon': r['weather'][0]['icon'],
    }
    context = {
        "article": instance, "comments": comments, "comment_form": comment_form, "obj": obj, "category": category,
        "related_post": related_post, 'categories': categories, "cate_1": cate_1, "cate_2": cate_2,
        "city_weather": city_weather, "cate_more": cate_more
    }
    return render(request, 'video-details.html', context)


def authorized_digital_sellers(request):
    return HttpResponse("google.com, pub-3270488626206393, DIRECT, f08c47fec0942fa0")




