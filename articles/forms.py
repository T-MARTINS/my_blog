from django import forms
from .models import NewsletterUser, Newsletter
from django.contrib.auth import (
    authenticate,
    get_user_model

 )

User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField(label='Username', widget=forms.TextInput(attrs={"placeholder": "Your Username"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder": "Your Password"}))

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError("This user does not exist")
            if not user.check_password(password):
                raise forms.ValidationError("Incorrect Password")
            if not user.is_active:
                raise forms.ValidationError("This user is not longer active")
        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(label="Email Address")
    email2 = forms.EmailField(label="Confirm Email")
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "email2",
            "password"
        ]

    def clean_email2(self):
        email = self.cleaned_data.get("email")
        email2 = self.cleaned_data.get("email2")
        if email != email2:
            raise forms.ValidationError("Email Must Match !")
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError("This Email has already been registered")
        return email


class NewsletterUserSignUpForm(forms.ModelForm):
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={"placeholder": "Your Email Address"}))

    class Meta:
        model = NewsletterUser
        fields = ['email']

        def clean_email(self):
            email = self.cleaned_data.get('email')

            return email


class NewsletterCreationForm(forms.ModelForm):
    subject = forms.CharField(label='Title')
    body = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Newsletter
        fields = ['subject', 'body', 'email', 'status']

