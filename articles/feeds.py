from django.contrib.syndication.views import Feed
from articles.models import Article


class LatestEntriesFeed(Feed):
    title = "Hipsterblog.com.ng: New Daily Updates from HipsterblogNews"
    link = "/feed/"
    description = "Updates on changes and additions on hipsterblog.com.ng"

    # return 10 recently created/updated posts
    def items(self):
        return Article.objects.all().order_by('-created_on')[:10]

    def item_title(self, item):
        return item.title

    # return a short description of article
    def item_description(self, item):
        return item.content

