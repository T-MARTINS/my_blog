from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from taggit.managers import TaggableManager
from comments.models import Comment


STATUS = (
    (0, "Draft"),
    (1, "Publish")
)


class User(AbstractUser):
    profile_image = models.ImageField(blank=True)
    cover_image = models.ImageField(blank=True)
    career = models.TextField(max_length=200, blank=True)
    bio = models.TextField(max_length=500, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    facebook_link = models.URLField(blank=True)
    twitter_link = models.URLField(blank=True)
    linkedin_link = models.URLField(blank=True)
    instagram_link = models.URLField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        db_table = 'user'

    def __str__(self):
        return '{}'.format(self.get_full_name())


class Category(models.Model):
    name = models.CharField(max_length=256, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.name)


class Article(models.Model):
    title = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(unique=True)
    image = models.ImageField(default='default.jpg', blank=True)
    category_ref = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=False,
                                     related_name='category', db_column='cat_table')
    author = models.ForeignKey(User, on_delete=models.SET_NULL,  null=True, default=1)
    content = RichTextUploadingField('content')
    tags = TaggableManager(blank=True)
    likecount = models.IntegerField(default=0, null=True, blank=True)
    viewcount = models.IntegerField(default=0, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=1)

    class Meta:
        ordering = ['-created_on']

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("articles:detail", kwargs={"slug": self.slug})

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type


class LikeViews(models.Model):
    action_choice = (
        ('like', 'like'),
        ('view', 'view')
    )
    ip = models.CharField(max_length=255, null=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, null=True, related_name='article_like')
    created_on = models.DateTimeField(auto_now_add=True)
    action = models.CharField(max_length=20, choices=action_choice, blank=True, null=True)

    def __str__(self):
        return ' Article: {}'.format(self.article.id)


class NewsletterUser(models.Model):
    email = models.EmailField()
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email


class Newsletter(models.Model):
    EMAIL_STATUS_CHOICES = (
        ("Draft", "Draft"),
        ("Published", "Published")
    )
    subject = models.CharField(max_length=255)
    body = RichTextUploadingField('body')
    email = models.ManyToManyField(NewsletterUser)
    status = models.CharField(max_length=10, choices=EMAIL_STATUS_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.subject


class Video(models.Model):
    title = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(unique=True)
    video_poster = models.ImageField(null=True, blank=True)
    video_url = models.URLField()
    category_ref = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=False,
                                 related_name='video_category', db_column='cat_table')
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, default=1)
    description = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("articles:video_detail", kwargs={"slug": self.slug})

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type

